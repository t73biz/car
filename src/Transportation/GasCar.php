<?php
namespace Transportation;

use Transportation\Car;

/**
 * Class GasCar
 * @package Transportation
 */
class GasCar extends Car
{
    /**
     * This will refuel the vehicle
     * @param $gallons
     */
    public function refuel($gallons)
    {
        if ($gallons <= 0 || $gallons > $this->fuelCapacity) {
            throw new \LogicException("The gallons of fuel should be greater than 1 and less than or equal to " . $this->fuelCapacity . ".");
        } else {
            $this->fuelLevel = $gallons;
        }
    }
}
