<?php
namespace Transportation;

use Transportation\Car;
/**
 * Class ElectricCar
 * @package Transportation
 */
class ElectricCar extends Car
{
    /**
     * Recharge the battery of the car.
     * @param $minutes
     */
    public function recharge($minutes)
    {
        if ($minutes <= 0 || $minutes > $this->fuelCapacity) {
            throw new \LogicException("The minutes of the recharge should be greater than 1 and less than  or equal to " . $this->fuelCapacity . ".");
        } else {
            $this->fuelLevel = $minutes;
        }
    }
}
