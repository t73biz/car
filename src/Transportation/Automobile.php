<?php
namespace Transportation;

/**
 * Interface Automobile
 * @package Transportation
 */
interface Automobile
{
    /**
     * This should return a percentage of the fuel remaining
     */
    public function getFuelLevel();

    /**
     * This should set the car in motion
     */
    public function drive();

    /**
     * This should stop the car's motion
     */
    public function stop();

    /**
     * This should return the motion state of the car.
     * @return boolean
     */
    function isMoving();
}
