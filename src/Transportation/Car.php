<?php
namespace Transportation;

use Transportation\Automobile;

/**
 * Class Car
 * @package Transportation
 */
class Car implements Automobile
{

    /**
     * The fuel capacity of the car.
     * @var integer $fuelCapacity
     */
    protected $fuelCapacity;

    /**
     * The current fuel level of the car. It defaults to 0.
     * @var integer $fuelLevel
     */
    protected $fuelLevel = 0;

    /**
     * Is the car moving?
     * @var boolean $moving
     */
    private $moving;

    /**
     * Set the fuelCapacity in the constructor
     * @param $fuelCapacity
     */
    public function __constructor($fuelCapacity)
    {
        // This prevents divide by 0 fatal errors
        if($fuelCapacity <= 0) {
            throw new \LogicException("The fuel capacity should be more than 0");
        }

        $this->fuelCapacity = $fuelCapacity;
    }

    /**
     * This returns the percentage of fuel or battery charge left in the car.
     */
    public function getFuelLevel()
    {
        if($this->fuelLevel == 0)
        {
            return 0;
        }

        return ($this->fuelCapacity / $this->fuelLevel)*100;
    }

    /**
     * Set the car in motion. Throws logic exceptions if the car can not move due to fuel or is already moving.
     * @throws \LogicException
     */
    public function drive()
    {
        if($this->fuelLevel == 0) {
            throw new \LogicException("The car can not move until it is recharged or refueled.");
        }

        if(!$this->isMoving()) {
            $this->moving = true;
        } else {
            throw new \LogicException("The car is already moving.");
        }
    }

    /**
     * Stop the car's motion. Throws logic exceptions if the car can not stop.
     * @throws \LogicException
     */
    public function stop()
    {
        if($this->isMoving()) {
            $this->moving = false;
        } else {
            throw new \LogicException("The car is already stopped.");
        }
    }

    /**
     * Check if the car is moving.
     * @return boolean
     */
    public function isMoving()
    {
        return $this->moving;
    }
}
